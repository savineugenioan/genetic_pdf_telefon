﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;

namespace Genetic_Lab_Nr_tel_PDF
{
    public partial class PDF : Form
    {
        Dictionary<string, string[]> lista;
        string[] files;
        int count;
        public PDF()
        {
            InitializeComponent();
            backgroundWorker1.DoWork += new DoWorkEventHandler(bkg);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lista = new Dictionary<string, string[]>();
            textBox1.Text = Properties.Settings.Default.GetPath;
            textBox2.Text = Properties.Settings.Default.PostPath;
            textBox3.Text = Properties.Settings.Default.XLPath;
            if (textBox1.Text.Trim()=="" || textBox2.Text.Trim() == "" || textBox3.Text.Trim() == "")
            {
                button1.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
            }
        }
        private bool checkPdf(string name)
        {
            return name.Contains(".pdf");
        }
        private void bkg(object sender, DoWorkEventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "")
            {
                MessageBox.Show($"Completati/Alegeti toate campurile de mai sus !", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                lista.Clear();
                createList();
                int cont = 0;
                if (count == 0)
                {
                    MessageBox.Show("Nu a fost gasit nici un fisier pdf !", "ATENTIE", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                foreach (var file in files)
                {
                    BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    if (checkPdf(file))
                    {
                        PdfReader reader = new PdfReader(file);
                        string key = file.Substring(0, file.IndexOf('_')).Substring(file.LastIndexOf('\\') + 1).Trim();
                        using (PdfStamper stamper = new PdfStamper(reader, File.Create(textBox2.Text + file.Substring(file.LastIndexOf("\\")))))
                        {
                            TextField tf = new TextField(stamper.Writer, new iTextSharp.text.Rectangle(200, 50, 100, 70), "Telefon")
                            {
                                FontSize = 14,
                            };
                            if (lista.ContainsKey(key) && lista[key][1] != "" && lista[key][1] != null)
                            {
                                string tel = lista[key][1].Substring(0, 1) == "0" ? lista[key][1] : string.Concat("0", lista[key][1]);
                                tf.Text = tel;
                            }
                            else
                            {
                                File.AppendAllText(textBox2.Text + "\\" + "fara_numar.txt", file + Environment.NewLine);
                            }
                                stamper.AddAnnotation(tf.GetTextField(), 1);

                            var pageSize = reader.GetPageSize(1);
                            int x = 50;
                            int y = 55;
                            ColumnText.ShowTextAligned(stamper.GetOverContent(1), Element.ALIGN_BOTTOM, new Phrase("Telefon:", new iTextSharp.text.Font(bf, 14)), x, y, 0);

                            stamper.Close();
                        }
                    }
                    cont++;
                    backgroundWorker1.ReportProgress(cont);
                }
                MessageBox.Show("Fisierele au fost editate cu succes", "Succes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                backgroundWorker1.ReportProgress(0);
            }
            catch (IOException exc)
            {
                MessageBox.Show("Va rog sa inchideti fisierele pdf deschise", "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("A aparut o eroare. Aceasta a fost scrisa in log", "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                File.AppendAllText(System.Reflection.Assembly.GetEntryAssembly().Location, ex.Message + Environment.NewLine);
            }
        }
        private void createList()
        {
            object misValue = System.Reflection.Missing.Value;
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(textBox3.Text);
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Excel.Range xlRange = xlWorksheet.UsedRange;
            int rw = xlRange.Rows.Count;
            int cl = xlRange.Columns.Count;
            for (int i = 1; i <= rw; i++)
            {
                var str1 = Convert.ToString((xlRange.Cells[i, 1] as Excel.Range).Value2);
                var str2 = Convert.ToString((xlRange.Cells[i, 2] as Excel.Range).Value2);
                var str6 = Convert.ToString((xlRange.Cells[i, 6] as Excel.Range).Value2);

                lista.Add(str1, new string[] { str2, str6 });
            }
            Marshal.ReleaseComObject(xlRange);
            Marshal.ReleaseComObject(xlWorksheet);
            xlWorkbook.Close(false, misValue, misValue);
            Marshal.ReleaseComObject(xlWorkbook);
            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            files = Directory.GetFiles(textBox1.Text);
            count = 0;
            foreach (var file in files)
            {
                if (file.Contains(".pdf"))
                    count++;
            }
            progressBar1.Maximum = count;
            progressBar1.Value = 0;
            if (File.Exists(textBox2.Text + "\\" + "fara_numar.txt"))
            {
                File.Delete(textBox2.Text + "\\" + "fara_numar.txt");
            }
            if (!backgroundWorker1.IsBusy)
            {
                Globalz.LoadingGif(this.button1);
                backgroundWorker1.RunWorkerAsync();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    textBox1.Text = fbd.SelectedPath;
                    Properties.Settings.Default.GetPath = fbd.SelectedPath;
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    textBox2.Text = fbd.SelectedPath;
                    Properties.Settings.Default.PostPath = fbd.SelectedPath;
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() == "" || textBox2.Text.Trim() == "")
            {
                button1.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() == "" || textBox2.Text.Trim() == "")
            {
                button1.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            using (var fbd = new OpenFileDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.FileName))
                {
                    textBox3.Text = fbd.FileName;
                    Properties.Settings.Default.XLPath = fbd.FileName;
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Globalz.del_gif(this);
            progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Globalz.del_gif(this);
            progressBar1.Value = 0;
        }
    }
}
